<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    event(new App\Events\RealTimeMessage('Hello World'));
//
//    return view('welcome');
//});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/chat_room/create', [App\Http\Controllers\HomeController::class, 'store']);

Route::resource('messages', App\Http\Controllers\MessageController::class);
Route::get('messages/index/{room_id}', [App\Http\Controllers\MessageController::class,'index'])->name('messages.index');
Route::get('/rooms', [App\Http\Controllers\MessageController::class,'getrooms'])->name('messages.rooms');
Route::get('/add-room-chat', [App\Http\Controllers\MessageController::class,'addroomform'])->name('room.chat.add.form');
Route::get('/room-chat', [App\Http\Controllers\MessageController::class,'addroom'])->name('room.chat.add');
Route::get('/add-user/{room_id}', [App\Http\Controllers\MessageController::class,'adduserroomform'])->name('add.user');
Route::get('/add-user1', [App\Http\Controllers\MessageController::class,'adduserroom'])->name('add.room.user');
