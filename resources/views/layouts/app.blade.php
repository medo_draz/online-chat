<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="user-id" content="{{ auth()->user()->id ?? '' }}">
    <meta name="user-name" content="{{ auth()->user()->name ?? '' }}">

    <title>Online Chat</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Font Awesome File -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Styles -->
{{--        <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <style>
        .select2-selection__choice__remove {
            width: auto !important;
        }

        .select2-selection__clear {
            width: auto !important;
        }

        .select2-selection--multiple {
            width: 566px !important;
        }

        .select2-dropdown--below {
            height: auto !important;
        }
    </style>
</head>
<body>
<div class="container app">
    @yield('content')

</div>
{{--<script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>--}}
{{--<script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>--}}
<link rel="stylesheet" href="{{ asset('plugins/noty/noty.css') }}">
<script src="{{ asset('plugins/noty/noty.min.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
@yield('script')

</body>
</html>
