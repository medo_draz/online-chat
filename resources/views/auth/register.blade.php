<!doctype html>
<html lang="en">
<head>
    <title>Sign Up - Online Chat</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('css/login_style.css') }}">

</head>
<body>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="color:red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5">
                <div class="login-wrap p-4 p-md-5">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="fa fa-user-o"></span>
                    </div>
                    <h3 class="text-center mb-4">Sign Up</h3>
                    <form action="{{ route('register') }}" method="Post" class="login-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{--                        <div class="form-group">--}}
                        {{--                            <input type="text" class="form-control rounded-left" name="first_name" placeholder="First Name" required>--}}
                        {{--                        </div>--}}
                        <div class="form-group">
                            <input type="text" class="form-control rounded-left" name="name" placeholder="User Name" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control rounded-left" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control rounded-left" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group d-flex">
                            <input type="password" class="form-control rounded-left" name="password_confirmation" placeholder="Confirm Password" required>
                        </div>
                        <div class="form-group d-flex">
                            <input type="file" class="form-control rounded-left" name="image" accept="image/*">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary rounded submit px-3">Register</button>
                        </div>

                        <div class="form-group d-md-flex text-center">
                            <div class="text-center">
                                Already have an account? <a href="{{ route('login') }}">Sign in</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>

