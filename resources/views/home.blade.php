@extends('layouts.app')

@section('content')
    <div class="row app-one">
        <div class="col-lg-3 side" style="height: 100vh;">
            <div class="side-one">
                <!-- Heading -->
                <div class="row heading">
                    <div class="col-sm-5 col-xs-3 heading-avatar" style="display: flex;">
                        <div class="heading-avatar-icon" style="display: flex;">
                            @if(auth()->user()->image)
                                <img src="{{asset('images/user_images/'.auth()->user()->image)}}">
                            @else
                                <img src="{{asset('images/default.jpeg')}}">
                            @endif
                            <span
                                style="margin-left: 15px;padding-top: 10px;">{{auth()->user()->name}}</span>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-1  heading-dot dropdown pull-right">
                        <i class="fa fa-ellipsis-v fa-2x dropdown-toggle pull-right" id="dropdownMenuButton"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-hidden="true"></i>
                        <div class="dropdown-menu" style="position: fixed !important;width: auto;height: auto;"
                             aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('room.chat.add.form') }}">
                                New Chat Group
                            </a>

                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#exampleModalCenter">
                                New Chat Group
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-2 heading-compose  pull-right">
                        <i class="fa fa-comments fa-2x  pull-right" aria-hidden="true"></i>
                    </div>

                </div>
                <!-- Heading End -->

                <!-- SearchBox -->
                <div class="row searchBox">
                    <div class="col-sm-12 searchBox-inner">
                        <div class="form-group has-feedback">
                            <input id="searchText" type="text" class="form-control" name="searchText"
                                   placeholder="Search">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                </div>
                <!-- Search Box End -->
                <!-- sideBar -->
                <span class="heading-name-meta" style="display: initial;">Friends</span>
                <ul class="list-group" id="online-users" style="height: 45%;overflow: auto;margin-bottom: 0;">
                    @if(isset($friends))
                        @foreach($friends as $friend)
                            <li class="list-group-item user-list"
                                onclick="getMessages({{$friend->chat_room_id}},'private',{{$friend->id}})"
                                id="friend-{{$friend->id}}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        @if($friend->image)
                                            <img src="{{asset('images/user_images/'.$friend->image)}}">
                                        @else
                                            <img src="{{asset('images/user_images/default.jpeg')}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span
                                                class="name-meta">{{$friend->name}}</span>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                            <span class="fa fa-circle text-danger"
                                                  id="friend-status-{{$friend->id}}"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @else
                    @endif
                </ul>
                <hr style="margin-top: 8px;margin-bottom: 8px;">
                <span class="heading-name-meta" style="display: initial;">Chat Groups</span>
                <ul class="list-group" id="chat-group" style="height: 34%;overflow: auto;">
                    @if(isset($chat_groups))
                        @foreach($chat_groups as $chat_group)
                            <li class="list-group-item user-list"
                                onclick="getMessages({{$chat_group->id}},'group','')"
                                id="chat_group-{{$chat_group->id}}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        @if($friend->image)
                                            <img src="{{asset('images/group_images/'.$chat_group->image)}}">
                                        @else
                                            <img src="{{asset('images/group_images/group_default.png')}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span class="name-meta">{{$chat_group->name}}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @else
                    @endif
                </ul>
                <!-- Sidebar End -->

            </div>
            <div class="side-two" id="side-two">

                <!-- Heading -->
                <div class="row newMessage-heading">
                    <div class="row newMessage-main">
                        <div class="col-sm-2 col-xs-2 newMessage-back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-10 col-xs-10 newMessage-title">
                            New Chat
                        </div>
                    </div>
                </div>
                <!-- Heading End -->
                <!-- ComposeBox -->
                <div class="row composeBox">
                    <div class="col-sm-12 composeBox-inner">
                        <div class="form-group has-feedback">
                            <input id="composeText" type="text" class="form-control" name="searchText"
                                   placeholder="Search People">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                </div>
                <!-- ComposeBox End -->

                <!-- sideBar -->
                <div class="row compose-sideBar" id="all-users">
                    @if(isset($users))
                        {{--                        {{$friends_id}}--}}
                        @foreach($users as $user)
                            {{--                            @if(in_array($user->id, $friends_id))'old'@else'new'@endif--}}
                            <div class="row sideBar-body"
                                 onclick="getMessages({{$user->chat_room_id}},'private',{{$user->id}})"
                                 id="user-{{$user->id}}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        @if($user->image)
                                            <img src="{{asset('images/user_images/'.$user->image)}}">
                                        @else
                                            <img src="{{asset('images/user_images/default.jpeg')}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span
                                                class="name-meta">{{ $user->name }}</span>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                            <span class="fa fa-circle text-danger"
                                                  id="user-status-{{$user->id}}"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                    @endif

                </div>
            </div>
            <!-- Sidebar End -->
        </div>
        <!-- New Message Sidebar End -->
        <div class="col-lg-9 empty-div" id="empty-div"></div>

        <!-- Conversation Start -->
        <div class="col-lg-9 conversation" id="conversation" style="height: 100vh;display:none;">
            <!-- Heading -->
            <div class="row heading">
                <div class="col-sm-2 col-md-1 col-xs-3 heading-avatar">
                    <div class="heading-avatar-icon">
                        <img id="friend_image" src="{{asset('images/default.jpeg')}}" style="width: 40px;">
                    </div>
                </div>
                <div class="col-sm-8 col-xs-7 heading-name">
                    <span class="heading-name-meta" id="friend_name">Ankit Jain</span>
                    <span class="heading-online">Online</span>
                </div>
                <div class="col-sm-1 col-xs-1  heading-dot pull-right">
                    <i class="fa fa-ellipsis-v fa-2x  pull-right" aria-hidden="true"></i>
                </div>
            </div>
            <!-- Heading End -->

            <!-- Message Box -->
            <div class="h-100 row message" id="messages"
                 style="overflow-y: scroll;padding-bottom: 15px !important;padding-top: 15px !important;">

                <div class="row message-body">
                    <div class="col-sm-12 message-main-receiver">
                            <span style="float: left;
                            text-align: start;
                            color: #9a9a9a;
                            margin-left: 10px;">sender</span>
                        <div class="receiver">
                            <div class="message-text">
                                Hyy, Its Awesome..!
                            </div>
                            <span class="message-time pull-right">Sun</span>
                        </div>
                    </div>
                </div>
                <div class="row message-body">
                    <div class="col-sm-12 message-main-sender">
                            <span style="float: right;
                            text-align: end;
                            color: #9a9a9a;
                            margin-right: 10px;">You</span>
                        <div class="sender">
                            <div class="message-text">
                                Thanks n I know its awesome...!
                            </div>
                            <span class="message-time pull-right">Sun</span>
                        </div>
                    </div>
                </div>


            </div>
            <!-- Message Box End -->

            <!-- Reply Box -->
            <div class="row reply">
                <div class="col-sm-1 col-xs-1 reply-emojis">
                    <i class="fa fa-smile-o fa-2x"></i>
                </div>
                <div class="col-sm-10 col-xs-9 reply-main">
                    <textarea class="form-control" rows="1" data-url="{{ route('messages.store') }}"
                              id="chat-text"></textarea>
                </div>
                {{--                <div class="col-sm-1 col-xs-1 reply-recording">--}}
                {{--                    <i class="fa fa-microphone fa-2x" aria-hidden="true"></i>--}}
                {{--                </div>--}}
                <input type="hidden" id="room_id" name="room_id" value=""/>
                <div class="col-sm-1 col-xs-1 reply-send" data-url="{{ route('messages.store') }}" id="send">
                    <i class="fa fa-send fa-2x" aria-hidden="true"></i>
                </div>
            </div>
            <!-- Reply Box End -->
        </div>
        <!-- Conversation End -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content" style="height: auto;">
                    <div class="modal-header" style="height: auto;">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ url('/chat_room/create') }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body" style="height: auto;">
                            {{ csrf_field() }}
                            <div class="form-group">
                                {{--                                <input type="hidden" name="user_id" value="{{auth()->user()->id}}">--}}
                                <input type="hidden" name="chat_type" value="group">
                                <input type="hidden" name="chat_room_id" value="0">
                                <label>name:</label>
                                <input type="text" name="name" class="form-control"
                                       value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <input type="file" class="form-control" name="image" id="image" accept="image/*">
                            </div>
                            <div class="form-group">
                                <label class="form-label select-label">Select Users:</label>
                                <select class="form-control select2 js-select2" name="users[]" id="select2"
                                        multiple="multiple">
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer" style="height: auto;">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $('.select2').select2({
            closeOnSelect: false,
            placeholder: "Click to select an Users",
            allowHtml: true,
            allowClear: true,
            tags: true
        })
        let onlineuserslength = 0;
        Echo.join(`online-users`)
            .here((users) => {
                onlineuserslength = users.length;
                let userId = $('meta[name=user-id]').attr('content');
                users.forEach(function (user) {
                    if (user.id === Number(userId)) {
                        return false;
                    }
                    document.getElementById('user-status-' + user.id).classList.remove("text-danger")
                    document.getElementById('user-status-' + user.id).classList.add("text-success")
                    let friend = document.getElementById('friend-status-' + user.id)
                    if (friend) {
                        document.getElementById('friend-status-' + user.id).classList.remove("text-danger")
                        document.getElementById('friend-status-' + user.id).classList.add("text-success")
                    }
                });
            }).joining((user) => {
            onlineuserslength++;
            let new_user = document.getElementById('user-' + user.id)
            // console.log(new_user)
            if (new_user) {
                document.getElementById('user-status-' + user.id).classList.remove("text-danger")
                document.getElementById('user-status-' + user.id).classList.add("text-success")
                let friend = document.getElementById('friend-status-' + user.id)
                if (friend) {
                    document.getElementById('friend-status-' + user.id).classList.remove("text-danger")
                    document.getElementById('friend-status-' + user.id).classList.add("text-success")
                }
            } else {
                $('#online-users').append(`
                        <li class="list-group-item user-list" onclick="getMessages(${user.id},'new')" id="user-${user.id}">
                            <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                <div class="avatar-icon">
                                    <img src="/images/user_images/${user.image}">
                                </div>
                            </div>
                            <div class="col-sm-9 col-xs-9 sideBar-main">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-8 sideBar-name">
                                        <span class="name-meta">${user.first_name} ${user.last_name}</span>
                                    </div>
                                    <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                        <span class="fa fa-circle text-success"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    `);
            }
        }).leaving((user) => {
            onlineuserslength--;
            // $('#user-' + user.id).remove();
            document.getElementById('user-status-' + user.id).classList.remove("text-success")
            document.getElementById('user-status-' + user.id).classList.add("text-danger")
            let friend = document.getElementById('friend-status-' + user.id)
            if (friend) {
                document.getElementById('friend-status-' + user.id).classList.remove("text-success")
                document.getElementById('friend-status-' + user.id).classList.add("text-danger")
            }
        })

        $('#send').on('click', function (e) {
            e.preventDefault();
            let body = $('#chat-text').val();
            let room_id = $('#room_id').val();
            let url = $(this).data('url');
            let data = {
                '_token': $('meta[name=csrf-token]').attr('content'),
                body,
                'chat_room_id': room_id
            };
            // console.log(url)
            // return false;
            $.ajax({
                url: url,
                method: 'post',
                data: data,
                success: function (data) {
                    $('#chat-text').val('');
                    $('#messages').append(`
                        <div class="row message-body">
                            <div class="col-sm-12 message-main-sender">
                                <span style="float: right;text-align: end;color: #9a9a9a;margin-right: 10px;">
                                    You
                                </span>
                                <div class="sender">
                                    <div class="message-text">
                                        ${data.message.body}
                                    </div>
                                    <span class="message-time pull-right">Sun</span>
                                </div>
                            </div>
                        </div>`
                    );

                    let chatWindow = document.getElementById('messages');
                    var xH = chatWindow.scrollHeight;
                    chatWindow.scrollTo(0, xH);
                }
            });
        })

        function getMessages(id, chat_type, user_id) {
            // console.log($('#image').val().split("\\")[2])
            // return false;
            let userId = $('meta[name=user-id]').attr('content');
            document.getElementById('room_id').value = id
            change()
            $.ajax({
                url: '/chat_room/create',
                method: 'post',
                enctype: 'multipart/form-data',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "chat_room_id": id,
                    "chat_type": chat_type,
                    "user_id": user_id,
                    "image": $('#image').val(),
                    "users": $('#select2').val()
                },
                success: function (data) {
                    // console.log(data)
                    if (id === 0) {
                        if (chat_type === 'private') {
                            let user_status = document.getElementById("user-status-" + user_id).className.split(" ")[2]
                            $('#online-users').append(`
                                <li class="list-group-item user-list" onclick="getMessages(${data.friend.chat_room_id},'private',${data.friend.id})" id="friend-${data.friend.id}">
                                    <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                        <div class="avatar-icon">
                                            <img src="/images/user_images/${data.friend.image}">
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-xs-9 sideBar-main">
                                        <div class="row">
                                            <div class="col-sm-8 col-xs-8 sideBar-name">
                                                <span class="name-meta">${data.friend.name}</span>
                                            </div>
                                            <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                                <span class="fa fa-circle ${user_status}" id="friend-status-${data.friend.id}"></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            `);
                            // console.log(document.getElementById("user-status-"+user_id).className.split(" ")[2])
                            document.getElementById('user-' + user_id).removeAttribute("onclick")
                            document.getElementById('user-' + user_id).setAttribute("onclick", `getMessages(${data.friend.chat_room_id},'private',${data.friend.id})`)
                        } else {
                            $('#chat-group').append(`
                            <li class="list-group-item user-list" onclick="getMessages(${data.id},'group','')" id="chat_group-${data.id}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        <img src="/images/group_images/${data.image}">
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span class="name-meta">${data.name}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        `);
                        }
                    }
                    document.getElementById('side-two').style.left = '-100%'
                    document.querySelector('li').classList.remove("active")
                    // console.log(document.querySelectorAll('.user-list'))
                    let elements = document.querySelectorAll('.user-list')
                    for (var i = 0; i < elements.length; i++) {
                        elements[i].classList.remove('active')
                    }
                    // console.log(userId)
                    $('#messages').empty()
                    if (data.messages) {
                        data.messages.forEach(function (item) {
                            if (Number(userId) === item.user_id) {
                                $('#messages').append(`
                                <div class="row message-body">
                                    <div class="col-sm-12 message-main-sender">
                                        <span style="float: right;text-align: end;color: #9a9a9a;margin-right: 10px;">
                                            You
                                        </span>
                                        <div class="sender">
                                            <div class="message-text">
                                                ${item.body}
                                            </div>
                                            <span class="message-time pull-right">${new Date(item.created_at).toLocaleString()}</span>
                                        </div>
                                    </div>
                                </div>
                            `)
                            } else {
                                $('#messages').append(`
                                <div class="row message-body" >
                                    <div class="col-sm-12 message-main-receiver">
                                        <span style="float: left;text-align: start;color: #9a9a9a;margin-left: 10px;">
                                            ${item.user.name}
                                        </span>
                                        <div class="receiver">
                                            <div class="message-text">
                                                ${item.body}
                                            </div>
                                            <span class="message-time pull-right">${new Date(item.created_at).toLocaleString()}</span>
                                        </div>
                                    </div>
                                </div>
                            `)
                            }
                            // console.log(item)
                        })
                    }

                    if (chat_type === 'private') {
                        $('#friend_name').text(data.friend.name)
                        document.getElementById('friend_image').removeAttribute("src")
                        document.getElementById('friend_image').setAttribute("src", '/images/user_images/' + data.friend.image)
                        document.getElementById('friend-' + user_id).classList.add("active")
                    } else {
                        $('#friend_name').text(data.data.name)
                        document.getElementById('friend_image').removeAttribute("src")
                        document.getElementById('friend_image').setAttribute("src", '/images/group_images/' + data.data.image)
                        document.getElementById('chat_group-' + id).classList.add("active")
                    }
                    document.getElementById('conversation').style.display = 'block'
                    document.getElementById('empty-div').style.display = 'none'
                    let chatWindow = document.getElementById('messages');
                    var xH = chatWindow.scrollHeight;
                    chatWindow.scrollTo(0, xH);
                }
            });

        }

        function change() {
            let roomId = $('#room_id').val();
            Echo.private(`chatgroup.${roomId}`)
                .listen('MessageDelivered', e => {
                    // console.log(roomId);
                    // console.log(e);
                    $('#messages').append(`
                        <div class="row message-body" >
                            <div class="col-sm-12 message-main-receiver">
                                <span style="float: left;text-align: start;color: #9a9a9a;margin-left: 10px;">
                                    ${e.username}
                                </span>
                                <div class="receiver">
                                    <div class="message-text">
                                        ${e.message}
                                    </div>
                                    <span class="message-time pull-right">Sun</span>
                                </div>
                            </div>
                        </div>`
                    );
                    let chatWindow = document.getElementById('messages');
                    var xH = chatWindow.scrollHeight;
                    chatWindow.scrollTo(0, xH);
                });
        }

        Echo.private('events')
            .listen('RealTimeMessage', (e) =>{
                let userId = $('meta[name=user-id]').attr('content');
                if (e.chat_room.room_type === 'private') {
                    if (e.chat_room.user_id.split(",").includes(userId) && Number(e.user.id) !== Number(userId)) {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "sfdcscsczxczc",
                            timeout: 10000,
                            killer: true
                        }).show();
                        $('#online-users').append(`
                            <li class="list-group-item user-list" onclick="getMessages(${e.chat_room.id},'private',${e.user.id})" id="friend-${e.user.id}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        <img src="/images/user_images/${e.user.image}">
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span class="name-meta">${e.user.name}</span>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                            <span class="fa fa-circle text-success" id="friend-status-${e.user.id}"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        `);
                    }
                } else {
                    if (e.chat_room.user_id.split(",").includes(userId)) {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "sfdcscsczxczc",
                            timeout: 10000,
                            killer: true
                        }).show();
                        $('#chat-group').append(`
                            <li class="list-group-item user-list" onclick="getMessages(${e.chat_room.id},'group','')" id="chat_group-${e.chat_room.id}">
                                <div class="col-sm-3 col-xs-3 sideBar-avatar">
                                    <div class="avatar-icon">
                                        <img src="/images/group_images/${e.chat_room.image}">
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-9 sideBar-main">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-8 sideBar-name">
                                            <span class="name-meta">${e.chat_room.name}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        `);
                    }
                }
                // Swal.fire({
                //     position: 'top-end',
                //     icon: 'success',
                //     title: 'Your work has been saved',
                //     showConfirmButton: false,
                //     timer: 1500
                // })
                // console.log(e)
            });
    </script>
@endsection
