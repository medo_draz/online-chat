<?php

namespace App\Http\Controllers;

use App\Events\RealTimeMessage;
use App\Models\ChatRoom;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Models\User;
use App\Notifications\RealTimeNotification;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index1()
    {
        $user = User::first();

        $user->notify(new RealTimeNotification('Hello World'));
        return view('home');
    }

    public function index()
    {
        $users = User::where('id', '!=', auth()->user()->id)->select('*', DB::raw('0 as chat_room_id'))->get();
        $charRoomsPrivate = ChatRoom::where('room_type', 'private')->get();
        $charRoomsGroup = ChatRoom::where('room_type', 'group')->get();
        $friends = [];
        $friends_id = [];
        $chat_groups = [];
        foreach ($charRoomsPrivate as $item) {
            if (in_array(auth()->user()->id, explode(',', $item->user_id))) {
                foreach ($users as $user) {
                    if (in_array($user->id, explode(',', $item->user_id))) {
                        $user->chat_room_id = $item->id;
                        array_push($friends, $user);
                        array_push($friends_id, $user->id);
                    }
                }
            }
        }
        foreach ($charRoomsGroup as $item) {
            if (in_array(auth()->user()->id, explode(',', $item->user_id))) {
//                foreach ($users as $user) {
//                    if(in_array($user->id, explode(',', $item->user_id))) {
                array_push($chat_groups, $item);
//                        array_push($friends_id,$user->id);
//                    }
//                }
            }
        }
//        dd($users);
        return view('home', compact('users', 'friends_id', 'friends', 'chat_groups'));
    }

    public function store(Request $request)
    {
        $users = User::where('id', '!=', auth()->user()->id)->select('*', DB::raw('0 as chat_room_id'))->get();
        if ((int)$request->chat_room_id === 0) {
            if ($request->chat_type === 'private') {
                $roomMembers = [auth()->user()->id, $request->user_id];
                sort($roomMembers);
                $roomMembers = implode(',', $roomMembers);
                $chat_room = ChatRoom::create([
                    'name' => $request->chat_type,
                    'room_type' => $request->chat_type,
                    'user_id' => $roomMembers,
                    'image' => 'group_default.png'
                ]);
                $friend = '';
                foreach ($users as $user) {
                    if (in_array($user->id, explode(',', $chat_room->user_id))) {
                        $user->chat_room_id = $chat_room->id;
                        $friend = $user;
                    }
                }
            } else if ($request->chat_type === 'group') {
//                dd($request->image);
                $roomMembers = [auth()->user()->id];
                foreach ($request->users as $item) {
                    array_push($roomMembers, (int)$item);
                }
                sort($roomMembers);
//                dd(implode(',', $roomMembers));
                $roomMembers = implode(',', $roomMembers);
                $chat_room = new ChatRoom();
                $chat_room->name = $request->name;
                $chat_room->room_type = $request->chat_type;
                $chat_room->user_id = $roomMembers;
                if (isset($request->image)) {
                    Image::make($request->image)->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(public_path('images/group_images/' . $request->image->hashName()));
                }
                $chat_room->image = isset($request->image) ? $request->image->hashName() : 'group_default.png';
                $chat_room->save();
                $friend = '';
//                broadcast(new AddFriend($chat_room))->toOthers();
                event(new RealTimeMessage($chat_room,auth()->user()));
                return redirect()->route('home');
            }
//            dd($friend);
//            broadcast(new AddFriend($chat_room))->toOthers();
            $user = User::where('id',$request->user_id)->first();
            event(new RealTimeMessage($chat_room,auth()->user()));
            $messages = Message::with(['user'])->where('chat_room_id', (int)$request->chat_room_id)->get();
//            event(new AddFriend($chat_room));
            return response()->json(['data' => $chat_room, 'messages' => $messages, 'friend' => $friend]);
        } else {
            $chat_room = ChatRoom::where('id', (int)$request->chat_room_id)->first();
            $friend = '';
            if ($request->chat_type === 'private') {
                foreach ($users as $user) {
                    if (in_array($user->id, explode(',', $chat_room->user_id))) {
                        $user->chat_room_id = $chat_room->id;
                        $friend = $user;
                    }
                }
            }
//            dd($friend);
            $messages = Message::with(['user'])->where('chat_room_id', (int)$request->chat_room_id)->get();
            return response()->json(['data' => $chat_room, 'messages' => $messages, 'friend' => $friend]);
        }
    }
}
